# README #

This repository is used to fuzz Nginx with AFL

* Version 1.0

# Setup #

* Make sure the version of your nginx is 1.9.3
* Copy AFL_Prepare folder to your nginx root folder
* run 'python AFL_nginx.py'
