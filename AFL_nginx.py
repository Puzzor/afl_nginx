#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: Puzzor
# @Date:   2015-08-03 22:10:43
# @Last Modified by:   Puzzor
# @Last Modified time: 2015-08-11 22:29:39
import os,hashlib,shutil,getpass
def md5sum(filename):             
    fd = open(filename,"r")  
    fcont = fd.read()  
    fd.close()           
    fmd5 = hashlib.md5(fcont)  
    return fmd5.hexdigest()

def build():
	os.chdir("../")
	os.system("mkdir /home/`whoami`/nginx")
	os.system("AFL_HARDEN=1 && CC=afl-gcc ./configure  --prefix=/home/`whoami`/nginx/ --with-select_module")
	os.system("make -j10")
	os.system("make install")

def fuzz(AFL_number):
	#Prepare initial seed
	os.system("mkdir /home/`whoami`/nginx/in /home/`whoami`/nginx/out /home/`whoami`/nginx/dict")
	os.system("cp AFL_Prepare/seed/* /home/`whoami`/nginx/in/")
	os.system("cp AFL_Prepare/dict/* /home/`whoami`/nginx/dict/")
	os.chdir("/home/"+getpass.getuser()+"/nginx")
	fd=open("conf/nginx.conf")
	content=fd.read()
	#master fuzzer
	os.system('nohup afl-fuzz -i in -o out -t 4000+ -m 1G -M fuzzer000 -- sbin/nginx -c conf/nginx.conf -o 8080 -r @@ &')
	#Change port in conf file
	for i in range(0,AFL_number):
		cc=content.replace('8080',str(8080+i+1))
		fd=open("conf/fuzzer"+str(i)+".conf",'w+')
		fd.write(cc)
		fd.close()
		#Fuzz
		os.system('nohup afl-fuzz -i in -o out -t 4000+ -m 1G -x dict/ -S fuzzer'+str(i)+" -- sbin/nginx -c conf/fuzzer"+str(i)+".conf -o "+str(8080+i+1)+" -r @@ &")
		


if __name__ == '__main__':
	tip='''Please put this script to your nginx/AFL_Prepare folder
		'''
	print tip
	raw_input("Press Enter to continue")
	if(os.path.exists('../src/core/nginx.c')):
		if md5sum('../src/core/nginx.c') == "bc517fe60c0b6fe904e351e81208f374":#origin File
			print 'Copy modified file to origin folder'
			shutil.copy("nginx.c","../src/core/nginx.c")
			shutil.copy("nginx.conf","../conf/nginx.conf")
			print 'Now we wil try to build nginx, default folder is /home/your name /nginx/'
			build()
			print 'Now we will use afl to fuzz nginx, first please input the number of AFL to run'
			AFL_number =input()
			if isinstance(AFL_number,int):
				print 'Input Ok, number is %d'%AFL_number
				fuzz(AFL_number)	
			else:
				print 'Input Error'

		else:
			print "Nginx.c hash incorrect!\n Maybe the version you use is not 1.9.3\nTry to modify nginx.c manually.\n"
			exit()
	else:
		print "Nginx.c not exists! Ensure you put this script in the root directory.\n"
		exit()
